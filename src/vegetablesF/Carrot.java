/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetablesF;

/**
 *
 * @author Adeel Khilji
 */
public class Carrot extends Vegetable
{
    public Carrot(String color, double size)
    {
        super(color, size);
    }
  
    public boolean isRipe()
    {
        boolean ripe;
        ripe = (super.getSize() >= 1.5 && super.getColor() == "orange") ? true : false;
        
        return ripe;
    }
}
