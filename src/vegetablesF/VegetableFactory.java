/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetablesF;

/**
 *
 * @author Adeel Khilji
 */
public class VegetableFactory 
{
    private static VegetableFactory vegetableFactory;
    
    private VegetableFactory(){}
    
    public static VegetableFactory getInstance()
    {
        if(vegetableFactory == null)
        {
            vegetableFactory = new VegetableFactory();
        }
        return vegetableFactory;
    }
    
    public Vegetable getVegetable(VegetableType type, String color, double size)
    {
        Vegetable vegetable = null;
        switch(type)
        {
            case CARROT: vegetable = new Carrot(color, size);
            break;
            case BEET: vegetable =  new Beet(color, size);
            break;
        }
        return vegetable;
    }
}
