/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetablesF;

/**
 *
 * @author Adeel Khilji
 */
public class Beet extends Vegetable
{
    protected Beet(String color, double size)
    {
        super(color,size);
        
    }
    
    public boolean isRipe()
    {
        boolean ripe;
        ripe = (super.getSize() >= 2.0 && super.getColor() == "red") ? true : false;
        
        return ripe;
    }
}
