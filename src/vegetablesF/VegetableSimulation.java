/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetablesF;

/**
 *
 * @author Adeel Khilji
 */
public class VegetableSimulation 
{
    public static void main(String[] args)
    {
        VegetableFactory factory = VegetableFactory.getInstance();
        
        
        Carrot oneCarrot = new Carrot("orange", 3);
        
        Vegetable carrot = factory.getVegetable(VegetableType.CARROT,oneCarrot.getColor(),oneCarrot.getSize());
        
        System.out.println("CARROT IS RIPE? " + carrot.isRipe());
        
        Carrot anotherCarrot = new Carrot("red", 2);
        
        carrot = factory.getVegetable(VegetableType.CARROT,anotherCarrot.getColor(), anotherCarrot.getSize());
        
        System.out.println(("ANOTHER CARROT IS RIPE? ") + carrot.isRipe());
        
        
        Beet oneBeet = new Beet("red", 2);
        
        Vegetable beet = factory.getVegetable(VegetableType.BEET,oneBeet.getColor(),oneBeet.getSize());
        
        System.out.println("BEET IS RIPE? " + beet.isRipe());
        
        Beet anotherBeet = new Beet("green", 1.5);
        
        beet = factory.getVegetable(VegetableType.BEET,anotherBeet.getColor(), anotherBeet.getSize());
        
        System.out.println(("ANOTHER CARROT IS RIPE? ") + beet.isRipe());
    }
}
