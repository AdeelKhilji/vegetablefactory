/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetablesF;

/**
 *
 * @author Adeel Khilji
 */
public enum VegetableType 
{
    CARROT("CARROT"), BEET("BEET");
    
    public String displayName;
    VegetableType(String displayName)
    {
        this.displayName = displayName;
    }
    public String toString()
    {
        return displayName;
    }
}
