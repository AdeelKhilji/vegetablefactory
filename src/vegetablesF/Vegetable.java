/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetablesF;

/**
 *
 * @author Adeel Khilji
 */
public abstract class Vegetable
{
    private String name, color;//Declaring instance variable
    private double size;//Declaring instance variable

    /**
     * Constructor with three parameters
     * @param name String
     * @param size double
     * @param color String
     */
    protected Vegetable(String color,double size)
    {
        
        this.color = color;
        this.size = size;
    }
    /**
     * getName - getter method
     * @return String
     */
    public String getName()
    {
        return this.name;
    }
    /**
     * getColor - getter method
     * @return String
     */
    public String getColor()
    {
        return this.color;
    }
    /**
     * getSize - getter method
     * @return double
     */
    public double getSize()
    {
        return this.size;
    }
    /**
     * isRipe - signature of boolean method - abstract
     * @return boolean
     */
    public abstract boolean isRipe();
}
